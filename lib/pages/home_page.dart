import 'package:flutter/material.dart';
import 'package:loja_virtual/Widgets/cart_button.dart';
import 'package:loja_virtual/Widgets/custom_drawer.dart';
import 'package:loja_virtual/tabs/home_tab.dart';
import 'package:loja_virtual/tabs/lojas_tab.dart';
import 'package:loja_virtual/tabs/orders_tab.dart';
import 'package:loja_virtual/tabs/products_tab.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  final _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      physics: const NeverScrollableScrollPhysics(),
      children: [
        Scaffold(
          body: const HomeTab(),
          drawer: CustomDrawer(_pageController),
          floatingActionButton: const CartButton(),

        ),
        Scaffold(
          appBar: AppBar(
            backgroundColor: const Color.fromARGB(255, 4, 125, 141),
            title: const Text("Produtos"),
            centerTitle: true,
          ),
          drawer: CustomDrawer(_pageController),
          body: const ProductsTab(),
          floatingActionButton: const CartButton(),
        ),
        Scaffold(
          appBar: AppBar(
            title: const Text('Lojas'),
            backgroundColor: const Color.fromARGB(255, 4, 125, 141),
            centerTitle: true,
          ),
          body: const LojasTab(),
          drawer: CustomDrawer(_pageController),
        ),

        Scaffold(
          appBar: AppBar(
            title: const Text('Meus Pedidos'),
            backgroundColor: const Color.fromARGB(255, 4, 125, 141),
            centerTitle: true,
          ),
          body: const OrdersTab(),
            drawer: CustomDrawer(_pageController),
        ),
      ],
    );
  }
}
