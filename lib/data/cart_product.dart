import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:loja_virtual/data/product_data.dart';

class CartProduct{
  String? cId;

  String? category;
  String? pId;

  int? quantity;
  String? size;

  ProductData? productData;

  CartProduct();

  CartProduct.fromDocument(DocumentSnapshot document){
    cId = document.id;
    category = document['category'];
    pId = document['pid'];
    quantity = document['quantity'];
    size = document['size'];
  }

  Map<String, dynamic> toMap(){
    return {
      'category' : category,
      'pid': pId,
      'quantity': quantity,
      'size': size,
      'product': productData?.toResumedMap(),
    };
}

}