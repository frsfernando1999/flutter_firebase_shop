import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

// Model é um objeto que guarda os estados de alguma coisa
// (no caso atual, o usuario)
class UserModel extends Model {
  Map<String, dynamic> userData = {};

  bool isLoading = false;

  UserCredential? firebaseUser;
  User? testUser;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  static UserModel of(BuildContext context) => ScopedModel.of<UserModel>(context);

  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);
    _loadCurrentUser();
    notifyListeners();
  }

  void signUp(
      {required Map<String, dynamic> userData,
      required String pass,
      required VoidCallback onSuccess,
      required VoidCallback onFail}) async {
    isLoading = true;
    notifyListeners();

    _auth
        .createUserWithEmailAndPassword(
      email: userData['email'],
      password: pass,
    )
        .then((user) async{
      firebaseUser = user;
      testUser = firebaseUser?.user;

      _saveUserData(userData);

      onSuccess();
      isLoading = false;
      notifyListeners();
    }).catchError((onError) {
      onFail();
      isLoading = false;
      notifyListeners();
    });
  }

  Future<void> signIn(
      {required String email,
      required String senha,
      required VoidCallback onSuccess,
      required VoidCallback onFail}) async {
    isLoading = true;
    notifyListeners();
    _auth
        .signInWithEmailAndPassword(email: email, password: senha)
        .then((user) async{
      firebaseUser = user;

      testUser = firebaseUser?.user;

      await _loadCurrentUser();
      onSuccess();
      isLoading = false;
      notifyListeners();

    }).catchError((onError) {
      onFail();
      isLoading = false;
      notifyListeners();
    });
  }

  void signOut() async {
    await _auth.signOut();
    userData = {};
    firebaseUser = null;
    testUser = null;
    notifyListeners();
  }

  void recoverPassword(String email) {
    _auth.sendPasswordResetEmail(email: email);
  }

  bool isLoggedIn() {
    return testUser != null;
  }

  Future _saveUserData(Map<String, dynamic> userData) async {
    this.userData = userData;
    FirebaseFirestore.instance
        .collection('users')
        .doc(testUser?.uid)
        .set(userData);
  }

  Future _loadCurrentUser() async {
    if (testUser == null) {
      testUser = _auth.currentUser;
      if (testUser != null) {
        if (userData['name'] == null) {
          DocumentSnapshot docUser = await FirebaseFirestore.instance
              .collection('users')
              .doc(testUser?.uid)
              .get();
            userData = docUser.data() as Map<String, dynamic>;
        }
      }
      notifyListeners();
    }
  }

}
