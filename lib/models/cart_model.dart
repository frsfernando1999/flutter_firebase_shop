import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:loja_virtual/data/cart_product.dart';
import 'package:loja_virtual/models/user_model.dart';
import 'package:scoped_model/scoped_model.dart';

class CartModel extends Model {
  UserModel user;
  List<CartProduct> products = [];

  bool isLoading = false;

  String? coupomCode;
  int discount = 0;

  CartModel(this.user) {
    if (user.isLoggedIn()) {
      _loadCartItems();
    }
  }

  static CartModel of(BuildContext context) =>
      ScopedModel.of<CartModel>(context);

  void addCartItem(CartProduct cartProduct) {
    products.add(cartProduct);
    FirebaseFirestore.instance
        .collection('users')
        .doc(user.testUser?.uid)
        .collection('cart')
        .add(cartProduct.toMap())
        .then((reference) {
      // add retorna uma referencia, que será o ID do cart
      cartProduct.cId = reference.id;
    });

    notifyListeners();
  }

  void removeCartItem(CartProduct cartProduct) {
    FirebaseFirestore.instance
        .collection('users')
        .doc(user.testUser?.uid)
        .collection('cart')
        .doc(cartProduct.cId)
        .delete();

    products.remove(cartProduct);

    notifyListeners();
  }

  void decProduct(CartProduct cartProduct) {
    cartProduct.quantity = cartProduct.quantity! - 1;

    FirebaseFirestore.instance
        .collection('users')
        .doc(user.testUser?.uid)
        .collection('cart')
        .doc(cartProduct.cId)
        .update(cartProduct.toMap());
    notifyListeners();
  }

  void incProduct(CartProduct cartProduct) {
    cartProduct.quantity = cartProduct.quantity! + 1;

    FirebaseFirestore.instance
        .collection('users')
        .doc(user.testUser?.uid)
        .collection('cart')
        .doc(cartProduct.cId)
        .update(cartProduct.toMap());
    notifyListeners();
  }

  _loadCartItems() async {
    QuerySnapshot query = await FirebaseFirestore.instance
        .collection('users')
        .doc(user.testUser?.uid)
        .collection('cart')
        .get();

    products = query.docs.map((doc) => CartProduct.fromDocument(doc)).toList();
    notifyListeners();
  }

  void setCoupom(String? coupomCode, int discount) {
    this.coupomCode = coupomCode;
    this.discount = discount;
  }

  void updatePrices() {
    notifyListeners();
  }

  double getProductsPrice() {
    double price = 0.0;
    for (CartProduct c in products) {
      if (c.productData != null) {
        price += c.quantity! * c.productData!.price!;
      }
    }
    return price;
  }

  double getShipPrice() {
    return 9.99;
  }

  double getDiscount() {
    return getProductsPrice() * discount / 100;
  }

  Future<String?> finishOrder() async {
    if (products.length == 0) {
      return null;
    }
    isLoading = true;
    notifyListeners();

    double productsPrice = getProductsPrice();
    double shipPrice = getShipPrice();
    double discount = getDiscount();

    DocumentReference refOrder =
        await FirebaseFirestore.instance.collection('orders').add({
      'clientId': user.testUser?.uid,
      'products': products.map((cartProduct) => cartProduct.toMap()).toList(),
      'shipPrice': shipPrice,
      'productsPrice': productsPrice,
      'discount': discount,
      'totalPrice': productsPrice - discount + shipPrice,
      'status': 1,
    });
    await FirebaseFirestore.instance
        .collection('users')
        .doc(user.testUser?.uid)
        .collection('orders')
        .doc(refOrder.id)
        .set({
      'orderId': refOrder.id,
    });
    QuerySnapshot query = await FirebaseFirestore.instance
        .collection('users')
        .doc(user.testUser?.uid)
        .collection('cart')
        .get();

    for (DocumentSnapshot doc in query.docs) {
      doc.reference.delete();
    }

    products.clear();
    discount = 0;
    coupomCode = null;
    isLoading = false;
    notifyListeners();

    return refOrder.id;
  }
}
