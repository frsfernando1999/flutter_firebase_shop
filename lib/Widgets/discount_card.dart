import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja_virtual/models/cart_model.dart';

class DiscountCard extends StatelessWidget {
  const DiscountCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      child: ExpansionTile(
        title: Text(
          'Cupom de desconto',
          textAlign: TextAlign.start,
          style:
              TextStyle(fontWeight: FontWeight.w500, color: Colors.grey[500]),
        ),
        leading: const Icon(Icons.card_giftcard),
        trailing: const Icon(Icons.add),
        children: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: TextFormField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Digite seu cupom!',
              ),
              initialValue: CartModel.of(context).coupomCode ?? '',
              onFieldSubmitted: (text) {
                FirebaseFirestore.instance
                    .collection('coupons')
                    .doc(text)
                    .get()
                    .then((docSnap) {
                  if (docSnap != null) {
                    CartModel.of(context).setCoupom(text, docSnap['value']);
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content:
                            Text('Desconto de ${docSnap['value']}% aplicado!'),
                        backgroundColor: Theme.of(context).primaryColor,
                      ),
                    );
                  } else {
                    CartModel.of(context).setCoupom(null, 0);
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text('Cupom não existente'),
                      backgroundColor: Colors.redAccent,
                    ));
                  }
                }).catchError((onError) {
                  CartModel.of(context).setCoupom(null, 0);
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text('Cupom não existente'),
                    backgroundColor: Colors.redAccent,
                  ));
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
