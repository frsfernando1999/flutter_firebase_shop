import 'package:flutter/material.dart';
import 'package:loja_virtual/models/user_model.dart';
import 'package:loja_virtual/pages/login_screen.dart';
import 'package:loja_virtual/tiles/drawer_tile.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CustomDrawer extends StatefulWidget {
  const CustomDrawer(this.pageController, {Key? key}) : super(key: key);

  final PageController pageController;

  @override
  State<CustomDrawer> createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  @override
  Widget build(BuildContext context) {
    Widget _buildDrawerBack() => Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 203, 236, 241),
                Colors.white,
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
        );

    return Drawer(
      child: Stack(
        children: [
          _buildDrawerBack(),
          ListView(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 8, left: 8, top: 16),
                padding: const EdgeInsets.fromLTRB(0, 16, 8, 8),
                height: 170,
                child: Stack(
                  children: [
                    const Positioned(
                      top: 0,
                      left: 0,
                      child: Text(
                        'Comidas\nManeiras',
                        style: TextStyle(
                          fontSize: 34,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 0,
                      bottom: 0,
                      child: ScopedModelDescendant<UserModel>(
                          builder: (context, child, model) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Olá${!model.isLoggedIn() || model.userData['name'] == null ? ", seja bem vindo(a)" : ", ${model.userData['name']}"}!",
                              style: const TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.bold),
                            ),
                            GestureDetector(
                              child: Text(
                                !model.isLoggedIn()
                                    ? "Entre ou cadastre-se ->"
                                    : "Sair",
                                style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              onTap: () {
                                if (!model.isLoggedIn()) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => const LoginScreen()));
                                } else {
                                  Fluttertoast.showToast(
                                      msg: 'Saiu com sucesso!',
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.black45,
                                      fontSize: 16.0);
                                  model.signOut();
                                }
                              },
                            ),
                          ],
                        );
                      }),
                    ),
                  ],
                ),
              ),
              const Divider(),
              DrawerTile("Início", Icons.home, widget.pageController, 0),
              DrawerTile("Produtos", Icons.list, widget.pageController, 1),
              DrawerTile("Lojas", Icons.location_on, widget.pageController, 2),
              DrawerTile(
                  "Meus Pedidos", Icons.playlist_add_check, widget.pageController, 3),
            ],
          )
        ],
      ),
    );
  }
}
