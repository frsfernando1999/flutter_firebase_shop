import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja_virtual/data/cart_product.dart';
import 'package:loja_virtual/data/product_data.dart';
import 'package:loja_virtual/models/cart_model.dart';

class CartTile extends StatelessWidget {
  final CartProduct cartProduct;

  const CartTile(this.cartProduct, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget _buildContent() {
      CartModel.of(context).updatePrices();
      return GestureDetector(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.all(8),
              width: 120,
              child: Image.network(
                cartProduct.productData?.images![0],
              ),
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      cartProduct.productData!.title!,
                      style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 17),
                    ),
                    Text(
                      'Tamanho: ${cartProduct.size}',
                      style: const TextStyle(fontWeight: FontWeight.w300),
                    ),
                    Text(
                      'R\$ ${cartProduct.productData?.price?.toStringAsFixed(2)}',
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          onPressed: cartProduct.quantity! > 1 ? (){
                            CartModel.of(context).decProduct(cartProduct);
                          } : null,
                          icon: const Icon(Icons.remove),
                          color: Theme.of(context).primaryColor,
                        ),
                        Text(cartProduct.quantity.toString()),
                        IconButton(
                            onPressed: () {
                              CartModel.of(context).incProduct(cartProduct);
                            },
                            icon: const Icon(Icons.add),
                            color: Theme.of(context).primaryColor),
                        TextButton(
                            onPressed: () {
                              CartModel.of(context).removeCartItem(cartProduct);
                            },
                            child: Text(
                              'Remover',
                              style: TextStyle(color: Colors.grey[500]),
                            ))
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
        onTap: (){

        },
      );
    }

    return Card(
        margin: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
        child: cartProduct.productData == null
            ? FutureBuilder<DocumentSnapshot>(
                future: FirebaseFirestore.instance
                    .collection('products')
                    .doc(cartProduct.category)
                    .collection('itens')
                    .doc(cartProduct.pId)
                    .get(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    cartProduct.productData = ProductData.fromDocument(
                        snapshot.data as DocumentSnapshot);
                    return _buildContent();
                  } else {
                    return Container(
                      height: 70,
                      child: const CircularProgressIndicator(),
                      alignment: Alignment.center,
                    );
                  }
                })
            : _buildContent());
  }
}
